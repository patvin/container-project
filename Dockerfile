FROM busybox
MAINTAINER Patrik Vincej
ENTRYPOINT ["/bin/cat"]
CMD ["/etc/passwd"]